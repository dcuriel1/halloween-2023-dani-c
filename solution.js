
const spooks = ['🎃', '👻', '💀', '🕷', '🕸', '🦇'];
const treats = ['🍰', '🍬', '🍡', '🍭', '🍪', '🍫', '🧁', '🍩'];

const kids = [
    {
        name: 'Dani',
        age: 8,
        height: 155
    },
    {
        name: 'Laura',
        age: 12,
        height: 135
    },
    {
        name: 'Luis',
        age: 14,
        height: 165
    }
]

function kidsWantTricks() {

    let numberOfSpooks = 0;
    kids.map(kid => kid.name).forEach(kidName => numberOfSpooks += Math.floor(kidName.length / 2));
    kids.map(kid => kid.age).forEach(kidAge => {if(kidAge % 2 === 0){ numberOfSpooks = numberOfSpooks + 2}});
    numberOfSpooks = kids.map(kid => kid.height).reduce((total, kidHeight) => total + (Math.floor(kidHeight / 100)*3), numberOfSpooks);

    return getRandomElements(spooks, numberOfSpooks);
};
function kidsWantTreats() {

    let numberOfTreats = 0;
    kids.map(kid => kid.name).forEach(kidName => numberOfTreats += kidName.length);
    kids.map(kid => kid.age).forEach(kidAge => numberOfTreats += Math.floor(((kidAge > 10) ? 10 : kidAge) / 3) * 2);
    kids.map(kid => kid.height).forEach(kidHeight => numberOfTreats += Math.floor(((kidHeight > 150) ? 150 : kidHeight) / 50) * 3);

    return getRandomElements(treats, numberOfTreats);
};

function getRandomElements(elements,  amount){

    let elementsToShow = [];
    for(let i = 0; i < amount; i++){
        elementsToShow.push(elements[(Math.floor(Math.random() * elements.length))]);
    }
    return elementsToShow;
}

console.log("Spooks", kidsWantTricks());
console.log("Spooks", kidsWantTricks().length);
console.log("Treats", kidsWantTreats());
console.log("Treats", kidsWantTreats().length);